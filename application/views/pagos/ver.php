<!DOCTYPE html>
<html class='no-js' lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
    <title>Dashboard</title>
    <meta content='lab2023' name='author'>
    <meta content='' name='description'>
    <meta content='' name='keywords'>
    <link href="<? echo base_url('assets/stylesheets/application-a07755f5.css');?>" rel="stylesheet" type="text/css" /><link href="//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<? echo base_url('assets/images/favicon.ico');?>" rel="icon" type="image/ico" />
    
  </head>
  <body class='main page'>
    <!-- Navbar -->
    <div class='navbar navbar-default' id='navbar'>
      <a class='navbar-brand' href='#'>
        <i class='icon-code'></i>
        Francisco Chirinos
      </a>
    </div>
    <div id='wrapper'>
      <!-- Sidebar -->
      <section id='sidebar'>
        <i class='icon-align-justify icon-large' id='toggle'></i>
        <ul id='dock'>
          <li class='launcher'>
            <i class='icon-home'></i>
            <a href="<?=base_url('');?>">Escritorio</a>
          </li>
          <li class='launcher'>
            <i class='icon-user'></i>
            <a href="<?=base_url('');?>usuarios">Usuarios</a>
          </li>
          <li class='launcher'>
            <i class='icon-star'></i>
            <a href="<?=base_url('');?>favoritos">Favoritos</a>
          </li>
          <li class='active launcher'>
            <i class='icon-money'></i>
            <a href='<?=base_url('');?>pagos'>Pagos</a>
          </li>
        </ul>
      </section>
      <!-- Tools -->
      <section id='tools'>
        <ul class='breadcrumb' id='breadcrumb'>
          <li class='title'>Escritorio</li>
        </ul>
      </section>
      <!-- Content -->
      <div id='content'>
        <div class='panel panel-default'>
          <div class='panel-heading'>
            .
            <div class='panel-tools'>
              <div class='btn-group'>
                <a class='btn' href='./'>
                  <i class='icon-refresh'></i>
                </a>
                <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Toggle'>
                  <i class='icon-chevron-down'></i>
                </a>
              </div>
            </div>
          </div>
          <div class='panel-body'>
            
            
            <table class='table table-condensed'>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Importe</th>
                <th>Fecha</th>
              </tr>
            </thead>
            <tbody>
             <?php
            foreach($usuariospagos_consulta as $usuariospagos){
                ?>
              <tr>
                <td>
                <?php foreach($usuarios_consulta as $usuarios){
                    if($usuariospagos->codigousuario == $usuarios->codigousuario){
                        echo $usuarios->usuario;
                    }
                }
                ?>
                </td>
                <td>
                <?php foreach($pagos_consulta as $pagos){
                    if($pagos->codigopago == $usuariospagos->codigopago){
                        echo $pagos->importe;
                        echo "</td><td>";
                        echo $pagos->fecha;   
                    }
                }
                ?>
                
                </td>
                
              </tr>
              <?php
            }
            ?>
              </tbody>
          </table>
            
        
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <!-- Javascripts -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script><script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" type="text/javascript"></script><script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js" type="text/javascript"></script><script src="<? echo base_url('assets/javascripts/application-985b892b.js');?>" type="text/javascript"></script>
  </body>
</html>
