<!DOCTYPE html>
<html class='no-js' lang='en'>
  <head>
    <meta charset='utf-8'>
    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
    <title>Dashboard</title>
    <meta content='lab2023' name='author'>
    <meta content='' name='description'>
    <meta content='' name='keywords'>
    <link href="<? echo base_url('assets/stylesheets/application-a07755f5.css');?>" rel="stylesheet" type="text/css" /><link href="//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<? echo base_url('assets/images/favicon.ico');?>" rel="icon" type="image/ico" />
    
  </head>
  <body class='main page'>
    <!-- Navbar -->
    <div class='navbar navbar-default' id='navbar'>
      <a class='navbar-brand' href='#'>
        <i class='icon-code'></i>
        Francisco Chirinos
      </a>
    </div>
    <div id='wrapper'>
      <!-- Sidebar -->
      <section id='sidebar'>
        <i class='icon-align-justify icon-large' id='toggle'></i>
        <ul id='dock'>
          <li class='launcher'>
            <i class='icon-home'></i>
            <a href="<?=base_url('');?>">Escritorio</a>
          </li>
          <li class='launcher'>
            <i class='icon-user'></i>
            <a href="<?=base_url('');?>usuarios">Usuarios</a>
          </li>
          <li class='launcher'>
            <i class='icon-star'></i>
            <a href="<?=base_url('');?>favoritos">Favoritos</a>
          </li>
          <li class='active launcher'>
            <i class='icon-money'></i>
            <a href='<?=base_url('');?>pagos'>Pagos</a>
          </li>
        </ul>
      </section>
      <!-- Tools -->
      <section id='tools'>
        <ul class='breadcrumb' id='breadcrumb'>
          <li class='title'>Escritorio</li>
        </ul>
      </section>
      <!-- Content -->
      <div id='content'>
        <div class='panel panel-default'>
          <div class='panel-heading'>
            <a class='btn btn-success' data-toggle='tooltip' href='<?=base_url('pagos/ver') ?>' title='Zoom'>
                    <i class='icon-plus'></i> Ver Pagos
                  </a>
            <div class='panel-tools'>
              <div class='btn-group'>
                <a class='btn' href='./'>
                  <i class='icon-refresh'></i>
                </a>
                <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Toggle'>
                  <i class='icon-chevron-down'></i>
                </a>
              </div>
            </div>
          </div>
          <div class='panel-body'>
            
            
            <?=form_open(base_url().'pagos/add');?>
           <label for="usuario_id">Usuario</label>
           <select name="usuario_id" id="usuario_id" class="form-control" required>
             <option value="" selected></option>
              <?php 
              foreach($usuarios_consulta as $fila){
           ?>
              <option value="<?=$fila->codigousuario?>"><?=$fila->usuario?></option> 
            <?php 
              }
           ?>
            </select>
           <br>
           <label for="importe">Monto</label>
           <input name="importe" id="importe" placeholder="0.00" type="number" step="0.01" class="form-control" min="0.01" required>
            <br>
            <label for="fecha">fecha</label>
            <input type="date" name="fecha" id="fecha" class="form-control" value="<?=$date?>" min="<?=$date?>" required>
           <br>
           
           <input type="submit" id="subtmit" value="Guardar">
          
            <?php
                echo form_close();
                ?>
            
        
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <!-- Javascripts -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script><script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" type="text/javascript"></script><script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js" type="text/javascript"></script><script src="<? echo base_url('assets/javascripts/application-985b892b.js');?>" type="text/javascript"></script>
  </body>
</html>
