<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    
     function __construct()
     {
         parent::__construct();
         $this->load->model('usuarios_model');
     }
    
	public function index()
	{
        $data['usuarios_consulta'] = $this->usuarios_model->usuarios_consulta();
        $this->load->view('usuarios/index',$data);
	}

    //funcion para procesar el formulario
     public function add()
    {
    	//si se ha pulsado el botón submit validamos el formulario con codeIgniter
        if($this->input->post('submit'))
        {
             //hacemos las comprobaciones que deseemos en nuestro formulario
             $this->form_validation->set_rules('usuario','usuario','trim|required|min_length[2]|is_unique[usuarios.usuario]');
             $this->form_validation->set_rules('clave','clave','trim|required|min_length[3]');
             $this->form_validation->set_rules('edad','edad','trim|required|integer|min_length[2]');

             //validamos que se introduzcan los campos requeridos con la función de ci required
             $this->form_validation->set_message('required', 'Campo %s es obligatorio');
             //validamos el email con la función de ci valid_email
             $this->form_validation->set_message('valid_email', 'El %s no es v&aacute;lido');
             //comprobamos que se cumpla el mínimo de caracteres introducidos
             $this->form_validation->set_message('min_length', 'Campo %s debe tener al menos %s car&aacute;cteres');
             //comprobamos que se cumpla el máximo de caracteres introducidos
             $this->form_validation->set_message('regex_match', 'Campo %s debe tener menos 18 años');
             $this->form_validation->set_message('is_unique', 'El Usuario %s ya existe en nuestra base de datos');
            
             
             if (!$this->form_validation->run())
             {
                 $this->add();
             }
             //si pasamos la validación correctamente pasamos a hacer la inserción en la base de datos
             else 
             {
                 $usuario = $this->input->post('usuario'); 
                 $clave = $this->input->post('clave'); 
                 $edad = $this->input->post('edad'); 
                 
                 $this->usuarios_model->nuevo_usuario($usuario,$clave,$edad);
                 
                 redirect(base_url("usuarios"), "refresh");
             }
        }else{
            $this->load->view('usuarios/add');
        }
    } //add
    
    function editar($usuario)
     {
         if($this->input->post('submit'))
        {
             //hacemos las comprobaciones que deseemos en nuestro formulario
             $this->form_validation->set_rules('clave','clave','trim|required|min_length[3]');
             $this->form_validation->set_rules('edad','edad','trim|required|integer|min_length[2]');

             //validamos que se introduzcan los campos requeridos con la función de ci required
             $this->form_validation->set_message('required', 'Campo %s es obligatorio');
             //validamos el email con la función de ci valid_email
             $this->form_validation->set_message('valid_email', 'El %s no es v&aacute;lido');
             //comprobamos que se cumpla el mínimo de caracteres introducidos
             $this->form_validation->set_message('min_length', 'Campo %s debe tener al menos %s car&aacute;cteres');
             //comprobamos que se cumpla el máximo de caracteres introducidos
             $this->form_validation->set_message('regex_match', 'Campo %s debe tener menos 18 años');
             $this->form_validation->set_message('is_unique', 'El Usuario %s ya existe en nuestra base de datos');
            
             
             if (!$this->form_validation->run())
             {
                 echo "error";
                 //$this->index();
             }
             //si pasamos la validación correctamente pasamos a hacer la inserción en la base de datos
             else 
             {
                 $clave = $this->input->post('clave'); 
                 $edad = $this->input->post('edad'); 
                 
                 $this->usuarios_model->actualizar_usuario($usuario,$clave,$edad);
                 
                 redirect(base_url("usuarios"), "refresh");
             }
        }else{
             
             
             $data['obtener'] = $this->usuarios_model->obtener($usuario);
             
            $this->load->view('usuarios/modificar',$data);
        }
     }
    
    
     function borrar()
     {
         $usuario = $this->uri->segment(3);
         $delete = $this->usuarios_model->eliminar_usuario($usuario);
         
         $data['usuarios_consulta'] = $this->usuarios_model->usuarios_consulta();
         $this->load->view('usuarios/index',$data);
     }
}
/*fin del archivo comentarios*/