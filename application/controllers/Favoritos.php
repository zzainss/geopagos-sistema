<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favoritos extends CI_Controller {
    
    function __construct()
     {
         parent::__construct();
         //$this->load->model('usuarios_model');
         $this->load->model('favoritos_model');
     }

	public function index()
	{
        $data['usuarios_consulta'] = $this->favoritos_model->usuarios_consulta();
		$this->load->view('favoritos/index',$data);
	}
    
    function add()
    {
        $usuario = $this->input->post('usuario');
        $data['usuario'] = $this->input->post('usuario');
        $data['usuarios_consulta'] = $this->favoritos_model->usuarios_consulta();
        $codigousuario = $this->input->post('codigousuario');
        if($usuario != null){
            $obtener = $this->favoritos_model->obtener($usuario);
            foreach($obtener as $fila){
                $data['usuario_id'] = $fila->codigousuario;
                $codigousuario = $fila->codigousuario;
            }
        }
        $data['favoritos_consulta'] = $this->favoritos_model->favoritos_consulta($codigousuario);
        

        if($this->input->post('submit'))
        {
            $codigousuario = $this->input->post('codigousuario');
            $favorito = $this->input->post('favorito');
            if($favorito != null and $codigousuario != null){
                $this->favoritos_model->nuevo_favorito($codigousuario,$favorito);
                redirect($this->load->view('favoritos/add')); 
            } else{    
                $this->load->view('favoritos/add',$data);    
            }
            
        }else{
        if ($usuario == null){
            $this->load->view('favoritos');
        }else{
            $this->load->view('favoritos/add',$data);
        }
            
        //set_cookie('cookie_usuario','valor','3600'); 
        //echo get_cookie('cookie_usuario');
        //delete_cookie('cookie_usuario'); 
        }

    }
}
