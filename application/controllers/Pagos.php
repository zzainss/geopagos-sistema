<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagos extends CI_Controller {

    function __construct()
     {
         parent::__construct();
         //$this->load->model('usuarios_model');
         $this->load->model('pagos_model');
        date_default_timezone_set('America/caracas');
     }
    
	public function index()
	{
        $data['date'] = date('Y-m-d');
        $data['usuarios_consulta'] = $this->pagos_model->usuarios_consulta();
		$this->load->view('pagos/index',$data);
	}
    function add(){
        
        $data['date'] = date('Y-m-d');
        $data['usuarios_consulta'] = $this->pagos_model->usuarios_consulta();
        
        $importe = $this->input->post('importe');
        $fecha = $this->input->post('fecha');
        $usuario_id = $this->input->post('usuario_id');
        
        $this->pagos_model->nuevo_pago($importe,$fecha,$usuario_id);
        $this->load->view('pagos/index',$data); 
    }
    function ver(){
        if($this->input->post('submit'))
        {
            echo "hola";
        }else{
            $data['usuarios_consulta'] = $this->pagos_model->usuarios_consulta();
            $data['pagos_consulta'] = $this->pagos_model->pagos_consulta();
            $data['usuariospagos_consulta'] = $this->pagos_model->usuariospagos_consulta();
            $this->load->view('pagos/ver',$data); 
        }
    }
}
