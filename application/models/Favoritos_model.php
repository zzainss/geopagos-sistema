<?php 
class Favoritos_model extends CI_Model
{
     function __construct()
     {
        parent::__construct();
     }
     
     public function usuarios_consulta()
     {
        $this->db->select('codigousuario,usuario,edad');
        $this->db->from('usuarios');
        $query = $this->db->get();
        
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
     }
    
    public function favoritos_consulta($codigousuario)
     {
        $this->db->select('codigousuario,codigousuariofavorito');
        $this->db->from('favoritos');
        $this->db->where('codigousuario',$codigousuario);
        $query = $this->db->get();
        return $query->result();
        
        //if($query->num_rows() > 0 )
        //{
            
        //}
     }

    public function nuevo_favorito($codigousuario,$favorito)
     {
         $data = array(
         'codigousuario' => $codigousuario,
         'codigousuariofavorito' => $favorito
         );
         $this->db->insert('favoritos',$data);
     }
    
    
    
    
    function obtener($usuario) {

            $this->db->select('codigousuario,usuario,clave,edad');
            $this->db->from('usuarios');
            $this->db->where('usuario',$usuario);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        
    }
    
    function actualizar_usuario($usuario,$clave,$edad) {
        $data = array(
            'clave' => $clave,
            'edad' => $edad
        );
        $this->db->where('usuario', $usuario);
        return $this->db->update('usuarios', $data);
    }
    
    function eliminar_usuario($usuario)
     {
     $this->db->where('usuario',$usuario);
     return $this->db->delete('usuarios');
        
     $this->db->where('codigousuario',$usuario);
     return $this->db->delete('favoritos');
     }
    
    
}
 
/*fin del archivo comentarios model*/