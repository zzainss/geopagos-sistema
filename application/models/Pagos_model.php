<?php 
class Pagos_model extends CI_Model
{
     function __construct()
     {
        parent::__construct();
     }
     
     public function usuarios_consulta()
     {
        $this->db->select('codigousuario,usuario,edad');
        $this->db->from('usuarios');
        $query = $this->db->get();
        
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
     }
     public function pagos_consulta()
     {
        $this->db->select('codigopago,importe,fecha');
        $this->db->from('pagos');
        $query = $this->db->get();
        
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
     }
    
     public function usuariospagos_consulta()
     {
        $this->db->select('codigopago,codigousuario');
        $this->db->from('usuariospagos');
        $query = $this->db->get();
        
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
     }
    
    
   
     public function nuevo_pago($importe,$fecha,$usuario_id)
     {
         $data = array(
         'importe' => $importe,
         'fecha' => $fecha
         );
         $this->db->insert('pagos',$data);
         
         $codigopago = $this->db->insert_id();
         
         
         
         $data = array(
         'codigopago' => $codigopago,
         'codigousuario' => $usuario_id
         );
         $this->db->insert('usuariospagos',$data);
     }
    
    
    
    
}
 
/*fin del archivo comentarios model*/