<?php 
class Usuarios_model extends CI_Model
{
     function __construct()
     {
        parent::__construct();
     }

     
     public function usuarios_consulta($id = false)
    {
        if($id === false)
        {
            $this->db->select('usuario,edad');
            $this->db->from('usuarios');
        }else{
            $this->db->select('usuario,edad');
            $this->db->from('usuarios');
            $this->db->where('usuario',$usuario);
        }
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
    }

    public function nuevo_usuario($usuario,$clave,$edad)
     {
         $data = array(
         'usuario' => $usuario,
         'clave' => $clave,
         'edad' => $edad,
         );
         //aqui se realiza la inserción, si queremos devolver algo deberíamos usar delantre return
         //y en el controlador despues de $nueva_insercion poner lo que queremos hacer, redirigir,
         //envíar un email, en fin, lo que deseemos hacer
         $this->db->insert('usuarios',$data);
     }
    
    function obtener($usuario) {

            $this->db->select('usuario,clave,edad');
            $this->db->from('usuarios');
            $this->db->where('usuario',$usuario);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        
    }
    
    function actualizar_usuario($usuario,$clave,$edad) {
        $data = array(
            'clave' => $clave,
            'edad' => $edad
        );
        $this->db->where('usuario', $usuario);
        return $this->db->update('usuarios', $data);
    }
    
    function eliminar_usuario($usuario)
     {
     $this->db->where('usuario',$usuario);
     return $this->db->delete('usuarios');
        
     $this->db->where('codigousuario',$usuario);
     return $this->db->delete('favoritos');
     }
    
    
}
 
/*fin del archivo comentarios model*/