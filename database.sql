/*
MySQL Backup
Source Server Version: 5.7.17
Source Database: sistema
Date: 28/11/2017 20:24:15
*/


-- ----------------------------
--  Table structure for `favoritos`
-- ----------------------------
DROP TABLE IF EXISTS `favoritos`;
CREATE TABLE `favoritos` (
  `codigousuario` int(11) NOT NULL,
  `codigousuariofavorito` int(11) NOT NULL,
  KEY `codigousuario` (`codigousuario`),
  KEY `codigousuariofavorito` (`codigousuariofavorito`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `pagos`
-- ----------------------------
DROP TABLE IF EXISTS `pagos`;
CREATE TABLE `pagos` (
  `codigopago` int(11) NOT NULL AUTO_INCREMENT,
  `importe` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`codigopago`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `codigousuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `clave` varchar(150) NOT NULL,
  `edad` int(3) NOT NULL,
  PRIMARY KEY (`codigousuario`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `usuariospagos`
-- ----------------------------
DROP TABLE IF EXISTS `usuariospagos`;
CREATE TABLE `usuariospagos` (
  `codigopago` int(11) NOT NULL,
  `codigousuario` int(11) NOT NULL,
  KEY `codigousuario` (`codigousuario`),
  KEY `codigopago` (`codigopago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
